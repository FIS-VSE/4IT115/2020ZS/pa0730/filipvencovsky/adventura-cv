package cz.vse.java.xname.adventuracv.main;

/**
 * pozorovatel
 */
public interface Observer {

    /**
     * volá pozorovaný (předmět pozorování) při změně
     */
    void update();
}
