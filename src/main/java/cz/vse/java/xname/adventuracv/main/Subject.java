package cz.vse.java.xname.adventuracv.main;

/**
 * Pozorovaný/pozorovatelný objekt (observable)
 * Předmět pozorování (subject)
 */
public interface Subject {

    /**
     * registrace nového pozorovatele
     * @param observer
     */
    void register(Observer observer);

    void unregister(Observer observer);

    /**
     * metoda pro upozovnění na změnu
     * odešle všem pozorovatelům zprávu
     */
    void notifyObservers();
}
