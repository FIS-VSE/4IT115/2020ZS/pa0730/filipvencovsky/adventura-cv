package cz.vse.java.xname.adventuracv.main;

import cz.vse.java.xname.adventuracv.logika.Hra;
import cz.vse.java.xname.adventuracv.logika.IHra;
import cz.vse.java.xname.adventuracv.logika.Prostor;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.util.HashMap;
import java.util.Map;

public class HomeController extends GridPane implements Observer {

    @FXML
    private ImageView hrac;
    @FXML
    private ListView panelVychodu;
    @FXML
    private TextArea vystup;
    @FXML
    private TextField vstup;
    @FXML
    private Button odesli;

    private IHra hra;
    private Map<String, Point2D> souradniceProstoru = new HashMap<>();

    @FXML
    private void initialize() {
        hra = new Hra();
        vystup.appendText(hra.vratUvitani()+"\n\n");
        vystup.setEditable(false);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });

        hra.getHerniPlan().register(this);

        souradniceProstoru = createSouradniceProstoru();
        update();
    }

    private Map<String, Point2D> createSouradniceProstoru() {
        Map<String, Point2D> souradniceProstoru = new HashMap<>();
        souradniceProstoru.put("domeček", new Point2D(20,73));
        souradniceProstoru.put("les", new Point2D(84,38));
        souradniceProstoru.put("hluboký_les", new Point2D(149,80));
        souradniceProstoru.put("chaloupka", new Point2D(213,34));
        souradniceProstoru.put("jeskyně", new Point2D(149,145));
        return souradniceProstoru;
    }

    @FXML
    private void odeslaniVstupu() {
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
        vstup.clear();
    }

    private void zpracujPrikaz(String prikaz) {
        vystup.appendText("Příkaz: "+prikaz+"\n");
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            panelVychodu.setDisable(true);
        }
    }

    @Override
    public void update() {

        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();

        panelVychodu.getItems().clear();
        panelVychodu.getItems().addAll(aktualniProstor.getVychody());

        String nazevProstoru = aktualniProstor.getNazev();
        hrac.setLayoutX(souradniceProstoru.get(nazevProstoru).getX());
        hrac.setLayoutY(souradniceProstoru.get(nazevProstoru).getY());
    }

    public void vybranVychod(MouseEvent mouseEvent) {
        Prostor vybranyProstor = (Prostor) panelVychodu.getSelectionModel().getSelectedItem();
        zpracujPrikaz("jdi"+" "+vybranyProstor.getNazev());
    }
}
